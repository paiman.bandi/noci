const express = require("express");
const app = express();
const port = 3000;

app.get("/", (req, res) => {
  res.send("Noci!");
});

app.listen(port, () => {
  console.log(`Noci listening at http://localhost:${port}`);
});
